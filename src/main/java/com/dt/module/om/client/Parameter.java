package com.dt.module.om.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Parameter {
    private String[] sysgroup = null;

    private Map<String, Map<String, String>> groupmap = new HashMap<String, Map<String, String>>();

    private Map<String, String> mapparstrs = new HashMap<String, String>();

    public String[] getGroup() {
        List<String> list = new ArrayList<String>();
        Set<String> keySet = this.groupmap.keySet();
        for (String keyName : keySet)
            list.add(keyName.toString());
        return list.<String>toArray(new String[list.size()]);
    }

    private void formatParameters() {
        if (this.mapparstrs.containsKey("sys.group")) {
            this.sysgroup = getParameter("sys.group").replace("{", "")
                    .replace("}", "").split(",");
            for (int i = 0; i < this.sysgroup.length; i++)
                this.groupmap.put(this.sysgroup[i], new HashMap<String, String>());
            Set<Map.Entry<String, String>> set = this.mapparstrs.entrySet();
            Iterator<Map.Entry<String, String>> it = set.iterator();
            while (it
                    .hasNext()) {
                Map.Entry<String, String> entry = it
                        .next();
                Map<String, String> tmpmap = new HashMap<String, String>();
                if ((((String)entry.getKey()).split("\\.")).length == 2 &&
                        this.groupmap.containsKey(((String)entry.getKey()).split("\\.")[0])) {
                    tmpmap = this.groupmap.get(((String)entry.getKey()).split("\\.")[0]);
                    tmpmap.put(((String)entry.getKey()).split("\\.")[1],
                            entry.getValue());
                    this.groupmap.put(((String)entry.getKey()).split("\\.")[0], tmpmap);
                }
            }

        }
    }

    public void printParameters() {
        if (this.mapparstrs.size() == 0) {
            System.out.println("No Parameter.");
        } else {
            System.out.println("Read All Parameter:");
            Set<Map.Entry<String, String>> set = this.mapparstrs.entrySet();
            Iterator<Map.Entry<String, String>> it = set.iterator();
            while (it
                    .hasNext()) {
                Map.Entry<String, String> entry = it
                        .next();
                System.out.println(String.valueOf(entry.getKey()) + "=" + (String)entry.getValue());
            }
        }
    }

    public void printGroupValues() {
        Map<String, String> tmpmap = new HashMap<String, String>();
        System.out.print("Print group:\n");
        Set<Map.Entry<String, Map<String, String>>> set = this.groupmap.entrySet();
        Iterator<Map.Entry<String, Map<String, String>>> it = set
                .iterator();
        while (it.hasNext()) {
            Map.Entry<String, Map<String, String>> entry = it
                    .next();
            System.out.println(String.valueOf(entry.getKey()) + ":");
            tmpmap = entry.getValue();
            Set<Map.Entry<String, String>> set2 = tmpmap.entrySet();
            Iterator<Map.Entry<String, String>> it2 = set2.iterator();
            while (it2
                    .hasNext()) {
                Map.Entry<String, String> entry2 = it2
                        .next();
                System.out.println("         " + (String)entry2.getKey() + "=" +
                        (String)entry2.getValue());
            }
        }
        System.out.print("\n");
    }

    public boolean formatFileParameter(String fileurl) {
        File file = new File(fileurl);
        String line = null;
        List<String> list = new ArrayList<String>();
        try {
            BufferedReader bw = new BufferedReader(new FileReader(file));
            while ((line = bw.readLine()) != null) {
                if (line.replaceAll(" ", "").isEmpty() ||
                        line.replaceAll(" ", "").startsWith("#"))
                    continue;
                list.add(line.trim());
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] strs = new String[2];
        for (int i = 0; i < list.size(); i++) {
            if ((((String)list.get(i)).split("=")).length == 2) {
                strs = ((String)list.get(i)).split("=");
                this.mapparstrs.put(strs[0], strs[1]);
            }
        }
        if (this.mapparstrs.size() == 0)
            return false;
        formatParameters();
        return true;
    }

    public boolean formatArgsParameter(String[] args) {
        String str = "";
        for (int i = 0; i < args.length; i++)
            str = String.valueOf(str) + args[i] + " ";
        String[] strs = str.split(",");
        String[] partempstrs = new String[2];
        this.mapparstrs = new HashMap<String, String>();
        for (int j = 0; j < strs.length; j++) {
            partempstrs = strs[j].split("=");
            if (partempstrs.length == 2 && !partempstrs[1].trim().isEmpty())
                this.mapparstrs.put(partempstrs[0].trim(), partempstrs[1].trim());
        }
        formatParameters();
        return true;
    }

    public String getParameter(String str) {
        if (this.mapparstrs.containsKey(str))
            return this.mapparstrs.get(str);
        return " ";
    }

    public String[] listgroupkey() {
        List<String> list = new ArrayList<String>();
        Set<String> keySet = this.groupmap.keySet();
        for (String keyName : keySet)
            list.add(keyName.toString());
        return list.<String>toArray(new String[list.size()]);
    }

    public boolean hasParameter(String str) {
        if (this.mapparstrs.containsKey(str))
            return true;
        return false;
    }

    public boolean isNumber(String args) {
        return args.matches("-*\\d+\\.?\\d*");
    }
}

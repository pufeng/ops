package com.dt.module.om.client;

import javax.swing.*;

public class RecLog {
    private static JTextArea textArea;

    public static void init(JTextArea textArea_in) {
        textArea = textArea_in;
    }

    public static void add(String str) {
        System.out.print(str);
        textArea.append(str);
    }
}

package com.dt.module.om.client;



import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("Since15")
public class ClientGUI extends JFrame {
    private static final long serialVersionUID = 1L;

    private JPanel contentPane;

    private Client client = new Client();

    private String selecthost = "";

    private JComboBox comboBox;

    private Parameter pars;

    private JButton btnNewButton_5;

    private JButton statusButton;

    private JButton updatButton;

    private JButton btnNewButton;

    private JButton btnNewButton_1;

    private JButton btnNewButton_2;

    private JButton btnNewButton_4;

    private JButton btnNewButton_8;

    private JButton btnNewButton_9;

    private JButton btnNewButton_11;

    private JButton btnNewButton_12;

    private JButton btnNewButton_7;

    private JButton btnNewButton_6;

    private JTextArea textArea;

    private boolean isconnect = false;

    private JButton def1Button;

    private JButton startButton;

    private JButton stopButton;

    private JButton def2Button;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientGUI frame = new ClientGUI();
                    frame.setSize(800, 600);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public ClientGUI() {
        setIconImage(
                Toolkit.getDefaultToolkit()
                        .getImage(
                                ClientGUI.class
                                        .getResource("/com/sun/java/swing/plaf/windows/icons/Computer.gif")));
        setTitle("APP");
        this.pars = new Parameter();
        this.pars.formatFileParameter("app.conf");
        setDefaultCloseOperation(3);
        setBounds(100, 100, 450, 300);
        this.contentPane = new JPanel();
        this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(this.contentPane);
        this.contentPane.setLayout(new BorderLayout(0, 0));
        JPanel panel = new JPanel();
        this.contentPane.add(panel, "North");
        panel.setLayout(new FlowLayout(0, 5, 5));
        JLabel lblNewLabel = new JLabel("列表");
                panel.add(lblNewLabel);
        this.comboBox = new JComboBox<String>(this.pars.listgroupkey());
        panel.add(this.comboBox);

        this.btnNewButton_5 = new JButton("测试");
        this.btnNewButton_5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ClientGUI.this.selecthost = ClientGUI.this.comboBox.getSelectedItem().toString();
                ClientGUI.this.pars.formatFileParameter("app.conf");
                ClientGUI.this.client.init(ClientGUI.this.pars, ClientGUI.this.selecthost);

            }
        });

        JPanel panel_1 = new JPanel();
        panel_1.setBorder(BorderFactory.createTitledBorder(null, "操作列表", 4, 2, new Font("Dialog", 1, 12), new Color(51, 51, 51)));
        this.contentPane.add(panel_1, "West");
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[2];
        gbl_panel_1.rowHeights = new int[9];
        gbl_panel_1.columnWeights = new double[] { 0.0D, Double.MIN_VALUE };
        gbl_panel_1.rowWeights = new double[] { 0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D,
                0.0D, 0.0D, Double.MIN_VALUE };
        panel_1.setLayout(gbl_panel_1);
        this.btnNewButton = new JButton("端口测试");
        this.btnNewButton.setEnabled(true);
        this.btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selnode=ClientGUI.this.selecthost = ClientGUI.this.comboBox.getSelectedItem().toString();
                RecLog.add("#######Action Start "+selnode+"######\n");
                if(selnode==null||"".equals(selnode)){
                    RecLog.add("Not Select Node.\n");
                }else{
                    ClientGUI.this.pars.formatFileParameter("app.conf");
                    if(ClientGUI.this.pars.hasParameter(selnode+".port.tcp")){
                        String lists=ClientGUI.this.pars.getParameter(selnode+".port.tcp");
                        String[] list_arr=lists.split(",");
                        for(int i=0;i<list_arr.length;i++){
                            String line=list_arr[i].trim();
                            String[] line_arr=line.split(":");
                            if(line_arr.length==2){
                                String h=line_arr[0];
                                String p=line_arr[1];
                                int pi=0;
                                try {
                                    pi = Integer.parseInt(p);
                                } catch (NumberFormatException a) {
                                    a.printStackTrace();
                                }
                                Boolean res=TelnetUtil.telnet(h,pi,2000);
                                if(res){
                                    RecLog.add("Record "+line+",result success!\n");
                                }else{
                                    RecLog.add("Record "+line+",result failed!\n");
                                }
                            }else{
                                RecLog.add("Record "+line+" conf error\n");
                            }
                        }
                    }else{
                        RecLog.add("Tcp empty\n");
                    }

                    if(ClientGUI.this.pars.hasParameter(selnode+".port.udp")){
                        String lists=ClientGUI.this.pars.getParameter(selnode+".port.udp");
                        String[] list_arr=lists.split(",");
                        for(int i=0;i<list_arr.length;i++){
                            String line=list_arr[i].trim();
                            String[] line_arr=line.split(":");
                            if(line_arr.length==2){
                                String h=line_arr[0];
                                String p=line_arr[1];
                                int pi=0;
                                try {
                                    pi = Integer.parseInt(p);
                                } catch (NumberFormatException a) {
                                    a.printStackTrace();
                                }
                                RecLog.add(UdpUtil.portcheck(h,pi,3000)+"\n");

                            }else{
                                RecLog.add("Record "+line+" conf error\n");
                            }
                        }
                    }else{
                        RecLog.add("Udp empty\n");
                    }
                }
                RecLog.add("\n");

            }
        });
        GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
        gbc_btnNewButton.fill = 2;
        gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
        gbc_btnNewButton.gridx = 0;
        gbc_btnNewButton.gridy = 0;
        panel_1.add(this.btnNewButton, gbc_btnNewButton);

        this.btnNewButton_1 = new JButton("清屏");
        this.btnNewButton_1.setEnabled(true);
        this.btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });
        GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
        gbc_btnNewButton_1.fill = 2;
        gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
        gbc_btnNewButton_1.anchor = 11;
        gbc_btnNewButton_1.gridx = 0;
        gbc_btnNewButton_1.gridy = 1;
        panel_1.add(this.btnNewButton_1, gbc_btnNewButton_1);

        JScrollPane scrollPane = new JScrollPane();
        this.contentPane.add(scrollPane, "Center");
        this.textArea = new JTextArea();
        this.textArea.setBackground(Color.WHITE);
        this.textArea.setLineWrap(true);
        scrollPane.setViewportView(this.textArea);
        this.textArea.setCaretPosition(this.textArea.getText().length());
        RecLog.init(this.textArea);
        JPanel panel_2 = new JPanel();
        panel_2.setBorder(BorderFactory.createTitledBorder(null, "", 4, 2, new Font("Dialog", 1, 12), new Color(51, 51, 51)));
        this.contentPane.add(panel_2, "South");
        panel_2.setLayout(new FlowLayout(0, 5, 5));
    }
}

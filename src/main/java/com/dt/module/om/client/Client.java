package com.dt.module.om.client;

import ch.ethz.ssh2.*;


import java.io.IOException;
import java.io.InputStream;

public class Client {
    private String hostalias = "";

    private boolean autologin = false;

    private String ip = "";

    private String user = "";

    private String pwd = "";

    private int port = 22;

    private Connection conn;

    private Session sess;

    private int sesswaittime = 5;

    private Parameter pars;

    private String proxyHost = "";

    private int proxyPort = 0;

    private InputStream stdout;

    private InputStream stderr;

    private byte[] buffer = new byte[8192];

    public void setSesswaittime(int t) {
        this.sesswaittime = t;
    }

    public String getUser() {
        return this.user;
    }

    public String getPwd() {
        return this.pwd;
    }

    public String getIp() {
        return this.ip;
    }

    public boolean getAutologin() {
        return this.autologin;
    }

    private String getParsValue(String flag) {
        if (this.pars.hasParameter(String.valueOf(this.hostalias) + "." + flag))
            return this.pars.getParameter(String.valueOf(this.hostalias) + "." + flag);
        return this.pars.getParameter("*." + flag);
    }

    public void init(Parameter pars_in, String selecthost_in) {

    }



    public boolean sesscreate() {
        this.conn = new Connection(this.ip, this.port);
        if (!this.proxyHost.equals("") && this.proxyPort != 0)
            this.conn.setProxyData((ProxyData)new HTTPProxyData(this.proxyHost, this.proxyPort));
        try {
            this.conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
            RecLog.add("host connect error.\n");
            return false;
        }
        try {
            this.conn.authenticateWithPassword(this.user, this.pwd);
        } catch (IOException e) {
            e.printStackTrace();
            RecLog.add("host connect error.\n");
            return false;
        }
        RecLog.add("######################" + this.hostalias + "######################\nConnected.\n");
        return true;
    }



    public boolean canConn() {
        sesscreate();
        try {
            this.sess = this.conn.openSession();
        } catch (IOException e) {
            e.printStackTrace();
            RecLog.add("Connect error.\n");
            return false;
        }
        RecLog.add("Connect ok.\n");
        sessclose();
        return true;
    }

    public void stdPrint() throws IOException {
        while (true) {
            if (this.stdout.available() == 0 && this.stderr.available() == 0) {
                int conditions = this.sess.waitForCondition(
                        28, (

                                1000 * this.sesswaittime));
                if ((conditions & 0x1) != 0) {
                    RecLog.add("Timeout while waiting for data from peer.\n");
                    break;
                }
                if ((conditions & 0x10) != 0)
                    if ((conditions & 0xC) == 0)
                        break;
            }
            while (this.stdout.available() > 0) {
                this.stdout.toString();
                int len = this.stdout.read(this.buffer);
                if (len > 0)
                    RecLog.add(new String(this.buffer, 0, len));
            }
            while (this.stderr.available() > 0) {
                int len = this.stderr.read(this.buffer);
                if (len > 0)
                    RecLog.add(new String(this.buffer, 0, len));
            }
        }
    }

    public boolean executeCmdStr(String cmdstr) {
        try {
            this.sess = this.conn.openSession();
            this.stdout = this.sess.getStdout();
            this.stderr = this.sess.getStderr();
            RecLog.add("$>" + cmdstr + "\n");
            this.sess.execCommand(cmdstr);
            stdPrint();
        } catch (IOException e) {
            e.printStackTrace();
            RecLog.add("session timeout.\n");
            return false;
        }
        this.sess.close();
        return true;
    }

    public boolean timeWait(int t) {
        try {
            RecLog.add("$>timewait " + t + " second.\n");
            Thread.sleep((t * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void sessclose() {
        this.conn.close();
        RecLog.add("connect release,over.\n");
    }

    private boolean cmdstrProcess(String cmdstr) {
        if (cmdstr.trim().startsWith("timewait")) {
            String[] cmdstrs = cmdstr.split(",");
            if (cmdstrs.length == 2) {
                timeWait(Integer.parseInt(cmdstrs[1].trim()));
            } else {
                timeWait(3);
            }
        } else if (!executeCmdStr(cmdstr)) {
            return false;
        }
        RecLog.add("\n");
        return true;
    }


}

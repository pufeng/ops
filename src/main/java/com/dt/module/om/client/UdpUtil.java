package com.dt.module.om.client;


import java.io.IOException;
import java.net.*;

public class UdpUtil {
    public static void main(String[] args) throws UnknownHostException {
        System.out.println(UdpUtil.portcheck("114.114.114.114",53,5000));
        System.out.println(UdpUtil.portcheck("127.0.0.1",51,5000));
    }
    public static String ScannerPortisAlive(int port,String HostIP){
        String result="OPEN";
        try{
            Socket testPortSocket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(HostIP, port);
            testPortSocket.connect(socketAddress,2000);
            testPortSocket.setSoTimeout(2000);

            testPortSocket.close();
        }catch (IOException e) {
            System.out.println(e.getMessage());
            result = "CLOSE";
        }
        catch (Exception ee) {
            result = "CLOSE";
            System.out.println(ee.getMessage());

        }
        return result;
    }
    public static String portcheck(String host, int port, int timeOut) {
        String msg="unknow";
        boolean flag = false;
        String line=host+":"+port;
        DatagramSocket socket = null;
        byte[] data = host.getBytes();
        try {
            socket = new DatagramSocket(0);
            socket.setSoTimeout(timeOut);
            socket.setTrafficClass(0x04 | 0x10);
            socket.connect(new InetSocketAddress(host, port));
            socket.send(new DatagramPacket(data, data.length));
            while (true) {
                byte[] receive = new byte[4096];
                DatagramPacket dp = new DatagramPacket(receive, 4096);
                socket.receive(dp);
                if (dp != null && dp.getData() != null) {
                    System.out.println("receive data from remote!");
                    byte[] bs = dp.getData();
                    for (int i = 0; i < bs.length; i++) {
                    }
                    msg=line+" udp open!receive data from remote.";
                    flag = true;
                    break;
                }
            }
        }
        catch (PortUnreachableException e) {
            flag=false;
            msg=line+" udp maybe closed! socket Exception:"+e.getMessage();
        }catch (SocketException e) {
            flag=false;
            msg=line+" udp maybe closed! socket Exception:"+e.getMessage();
        } catch (SocketTimeoutException e) {
            flag=true;
            if(port==53){
                if("OPEN".equals(ScannerPortisAlive(53,host))){
                    msg=line+" udp open!";
                }else{
                    flag=false;
                    msg=line+" udp closed!";
                }
            }else{
                msg=line+" udp maybe open or filtered ! socket Exception:"+e.getMessage();
            }
        }catch (Exception ee) {
            flag=false;
            msg=line+" udp maybe closed! socket Exception:"+ee.getMessage();
            //ee.printStackTrace();
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Exception e) {
                System.out.println(line+" socket.close Exceptio");
                e.printStackTrace();
            }
        }
        return flag+"| "+msg;
    }

}
package com.dt.module.om.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.dt.module.om.Command;
import com.dt.module.om.term.entity.Machine;
import com.dt.ops.Main;

public class ToolUtil {

	public static ArrayList<Machine> readHostConf(String name) {
		ArrayList<Machine> ms = new ArrayList<Machine>();

		FileInputStream filestream = null;
		InputStreamReader readStream = null;
		BufferedReader reader = null;

		try {
			filestream = new FileInputStream(name);
			readStream = new InputStreamReader(filestream, "utf-8");
			reader = new BufferedReader(readStream);
			String temp = "";
			try {
				while ((temp = reader.readLine()) != null) {
					if (temp != null && temp.trim().length() > 0) {
						String trimtemp = temp.trim();
						if (!trimtemp.startsWith("#")) {
							String[] arr = trimtemp.split("\\|");
							if (arr.length == 5) {
								Machine m = new Machine();
								m.setHostname(arr[0].trim());
								m.setPort(Integer.parseInt(arr[1]));
								m.setUsername(arr[2].trim());

								if(Main.pwdentr){
									String p="idle";
									try {
										DES d=new DES();
										p=d.decrypt(arr[3].trim());
									} catch (Exception e) {
										System.out.println("encrypt/decrypt error "+e.getMessage());
									}
									m.setPassword(p);
								}else{
									m.setPassword(arr[3].trim());
								}

								m.parseKvMap(arr[4]);
								ms.add(m);
							}
						}
					}
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			if (readStream != null)
				try {
					readStream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			if (filestream != null)
				try {
					filestream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}
		return ms;
	}


	public static ArrayList<String> fil=new ArrayList<String>(){{
		add("dev/sd");
		add("dev/hd");
		add("dev/vd");
		add("resize");
		add("extend");
		add("shutdown");
		add("mkfs");
		add("remove");
		add("init");
		add("rm");
		add("fsck");
		add("delete");
		add("reduce");
	}};

	public static Command readCmdConf(String name) {

		ArrayList<String> cmds = new ArrayList<String>();
		ArrayList<String> uploads = new ArrayList<String>();
		ArrayList<String> download = new ArrayList<String>();
		FileInputStream filestream = null;
		InputStreamReader readStream = null;
		BufferedReader reader = null;
		try {
			filestream = new FileInputStream(name);
			readStream = new InputStreamReader(filestream, "utf-8");
			reader = new BufferedReader(readStream);
			String temp = "";

			while ((temp = reader.readLine()) != null) {
				if (temp != null && temp.trim().length() > 0) {
					// filter
					if (temp.startsWith("#")){
						//System.out.println("Mark Cmd:" + temp);
						continue;
					}
					if (temp.startsWith("UPLOAD")){
						uploads.add(temp.replaceFirst("UPLOAD",""));
						continue;
					}
					if (temp.startsWith("DOWNLOAD")){
						download.add(temp.replaceFirst("DOWNLOAD",""));
						continue;
					}
//					if( "dd".equals(temp.trim().toLowerCase()) ||temp.indexOf("/dd") >= 0 ){
//						System.out.println("Filter Cmd:" + temp);
//						continue;
//					}

					//关键字过滤
					String filaccess="1";
					if(temp!=null||temp.trim().startsWith("ULIMIT")){
						temp=temp.replaceFirst("ULIMIT","");
					}else{
						for(int i=0;i<fil.size();i++){
							String f=fil.get(i);
							if(temp.indexOf(f) >= 0){
								filaccess="0";
								break;
							}
						}
					}
					if("0".equals(filaccess)){
						System.out.println("Filter Cmd:" + temp);
						continue;
					}
					cmds.add(temp);
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (reader != null)
				try {
					reader.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			if (readStream != null)
				try {
					readStream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			if (filestream != null)
				try {
					filestream.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		}
		Command c=new Command();
		c.cmds=cmds;
		c.upload=uploads;
		c.download=download;
		return c;
	}

}

### 说明
- 使用方式
```
Usage:java -jar ops.jar [-c <arg>] [-dd <arg>] [-df <arg>] [-e <arg>] [-en <arg>] [-fil] [-ftp <arg>] [-g] [-h] [-m <arg>]
           [-n <arg>] [-portcheck] [-pwden] [-s <arg>] [-sd <arg>] [-sf <arg>] [-sftp <arg>] [-v]
      -c,--cmd <arg>             set configuration file of command describe(default: cmd.conf)
      -dd,--dest_dir <arg>       the sftp/ftp destination directory
      -df,--dest_filename <arg>  the sftp/ftp destination filename
      -e,--exec <arg>            set the execute type,support type is show,check,action
      -en,--encryption <arg>     encrypted string
      -fil,--filter              keyword ULIMIT to skip [dev/sd, dev/null, dev/hd, dev/vd, resize, extend, shutdown, mkfs,
                                 remove, init, rm, fsck, delete, reduce]
      -ftp,--ftp <arg>           the ftp function,support type is upload,download
      -g,--shellgui              start a simple ssh client
      -h,--help                  show help
      -m,--md5 <arg>             show file md5 value
      -n,--node <arg>            set configuration file of node describe(default: base.node)
      -portcheck,--portcheck     edit rule to check port is open or not
      -pwden,--pwden             if encrypted ftp/sftp/ssh
      -s,--src_path <arg>        the sftp/ftp source file
      -sd,--src_dir <arg>        the sftp/ftp source directory
      -sf,--src_filename <arg>   the sftp/ftp source filename
      -sftp,--sftp <arg>         the sftp function,support type is upload,download
      -v,--version               show version and exit
Priority:
        help-->version-shellgui-sftp-batchops
Example:
        java -jar ops.jar [-pwden] -e show -n base.node -c cmd.conf 
        java -jar ops.jar [-pwden] -n base.node -sftp upload    -s  /tmp/source.file  -df dest.file -dd /tmp
        java -jar ops.jar [-pwden] -n base.node -sftp download  -s  /tmp/file.txt     -df dest.file -dd /tmp
        java -jar ops.jar [-pwden] -n base.node -ftp  upload    -s  /tmp/source.file  -df dest.file -dd /tmp
        java -jar ops.jar [-pwden] -n base.node -ftp  download  -sd /tmp              -sf ftp.txt   -dd /tmp
        java -jar ops.jar -m /tmp/file
        java -jar ops.jar -portcheck
        java -jar ops.jar -en something 
        java -jar ops.jar -g 
        java -jar ops.jar -v 
        java -jar ops.jar -h 
```

- 配置文件node.conf
```
#ip|port|user|password
39.105.191.22|22|root|Root|var=1@@var=2
```

- 配置文件cmd.conf
```
##
#java -jar ops.jar -e action -n base.node -c cmd.conf
#
#UPLOAD   sf=cmd.conf     @@ dd=/tmp  @@ df=cmd.conf
#UPLOAD   sf=base.node    @@ dd=/tmp  @@ df=base.node
DOWNLOAD  sf=/tmp/2.file  @@ dd=/tmp  @@ df=dest.file
echo "start ##hostname##  ##hostname## " >/tmp/a.sh
ULIMIT echo "111  ##abc## ##ip## ">>/tmp/a.sh
echo "##var1##@@##var2##@@" >>/tmp/a.log
echo "222" >>/tmp/a.sh
echo "333" >>/tmp/a.sh
echo "4444">>/tmp/a.sh
shutdown
a=12
if [[ $a -eq "12" ]];then
  cd /tmp
  touch $$
fi
exit 0
```
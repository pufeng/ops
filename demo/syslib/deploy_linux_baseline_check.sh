#!/bin/sh
#version 1.2.3 modify at 20210412
#sh cmd.sh 1
action=0
if [[ -n $1 ]];then
	action=$1
fi 
echo ""
##############################################################
echo "########## check profile timeout"
c=`cat /etc/profile|grep -v "#"|grep TMOUT|wc -l`
if [[ $c -eq 0 ]];then
	echo "failed"
	if [[ $action -eq 1 ]];then
		echo "TMOUT=180">>/etc/profile
		echo "export TMOUT">>/etc/profile
		echo "set success"
	fi
else
	echo "success"
fi
##############################################################
echo "########## check rsyslog"
if [[ -f /etc/rsyslog.conf ]];then
	logcnt=`cat /etc/rsyslog.conf|grep -v "#"|grep 10.21.1.115|wc -l`
	if [[ $logcnt -eq 0 ]];then
		echo "failed"
		if [[ $action -eq 1 ]];then
			echo "*.* @10.21.1.115">>/etc/rsyslog.conf
			echo "set success"
		fi
	else
		echo "success" 
	fi
fi
##############################################################
echo "########## check system-auth"
if [[ -f /etc/pam.d/system-auth ]];then
	authcnt=`cat /etc/pam.d/system-auth|grep pam_tally.so|wc -l`
	if [[ $authcnt -eq 0 ]];then
		echo "failed"
		if [[ $action -eq 1 ]];then
			echo "auth required /lib/security/\$ISA/pam_tally.so onerr=fail no_magic_root      ">>/etc/pam.d/system-auth
			echo "account required /lib/security/\$ISA/pam_tally.so deny=6 no_magic_root reset ">>/etc/pam.d/system-auth
			echo "password requisite  pam_cracklib.so try_first_pass retry=3 difok=3 minlen=8 dcredit=-1 ucredit=-1 lcredit=-1 ocredit=-1" >>/etc/pam.d/system-auth
			echo "password sufficient pam_unix.so sha512 shadow nullok try_first_pass use_authtok remember=5" >>/etc/pam.d/system-auth
			echo "set success"
		fi
	else
		echo "success"
	fi
fi
##############################################################
echo "########## check PASS_MAX_DAYS"
dvalue=`cat /etc/login.defs|grep -v "#"|grep PASS_MAX_DAYS|awk '{print $NF}'`
if [[ $dvalue -eq 90 ]];then
	echo "success"
else
	echo "failed"
	if [[ $action -eq 1 ]];then
		sed -i '/PASS_MAX_DAYS/d' /etc/login.defs
		echo "PASS_MAX_DAYS 90">>/etc/login.defs
		echo "set success"
	fi
fi
##############################################################
echo "########## check PASS_MIN_LEN"
dvalue=`cat /etc/login.defs|grep -v "#"|grep PASS_MIN_LEN|awk '{print $NF}'`
if [[ $dvalue -eq 8 ]];then
	echo "success"
else
	echo "failed"
	if [[ $action -eq 1 ]];then
		sed -i '/PASS_MIN_LEN/d' /etc/login.defs
		echo "PASS_MIN_LEN 8">>/etc/login.defs
		echo "set success"
	fi
fi
##############################################################
echo "########## check zabbix agent autostart"
zpath=`which zabbix_agentd 2>&1>/dev/null`
if [[ $? -eq 0 ]];then
	echo "success"
	zcnt=`cat /etc/rc.d/rc.local|grep zabbix|wc -l`
	if [[ $zcnt -eq 0 ]];then
		chmod +x /etc/rc.d/rc.local
		echo "$zpath">>/etc/rc.d/rc.local
	fi
else
    echo "failed"
fi
##############################################################
echo "########## check selinux"
sediscnt=`cat /etc/selinux/config |grep -v "#"|grep disabled|wc -l`
if [[ $sediscnt -eq 1 ]];then
	echo "success"
else
	echo "failed"
	if [[ $action -eq 1 ]];then
		sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
	fi
fi
##############################################################
echo "########## check /etc/rc.d/rc.local"
cat /etc/rc.d/rc.local|grep -v "#"|grep -v "lock/subsys"
##############################################################
echo "########## check rhost"
echo "hosts.allow list"
cat /etc/hosts.allow|grep -v "#"
#echo "sshd:10.7.5.27:allow">>/etc/hosts.allow
echo "hosts.allow deny"
cat /etc/hosts.deny|grep -v "#"
#echo "sshd:all:deny">>/etc/hosts.deny
echo "########## check port"
netstat -ant|grep LISTEN
##############################################################
exit 0
